var app = angular.module('angLern', []);
app.controller('appController', ['$scope', '$http', function ($scope, $http) {

    $scope.data = {
        num1: 10,
        num2: 20,
        num3: 30
    };
    $scope.b = 20

    $scope.gender = {};
    $scope.dataGender = [];
    $scope.search = '';
    $scope.genderId = 0;

    $scope.getGender = function () {
        $http({
            url: 'http://softendev.lpru.ac.th/~prostd57115/data.php?page=getData',
            method: "GET",
        }).then(function (res) {

            $scope.dataGender = res.data;

        }, function errorCallback(err) {
            console.log(err);
        });
    }

    $scope.addGender = function () {
        if ($scope.genderId != 0) {
            $http({
                url: 'http://softendev.lpru.ac.th/~prostd57115/data.php?page=edit&id=' + $scope.genderId,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    gender: $scope.gender
                })
            }).then(function (res) {
                $scope.getGender();
                $scope.renderChart();
            }, function errorCallback(err) {
                console.log(err);
            });
        } else {
            $http({
                url: 'http://softendev.lpru.ac.th/~prostd57115/data.php?page=add',
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    gender: $scope.gender
                })
            }).then(function (res) {
                $scope.getGender();
                $scope.renderChart();
            }, function errorCallback(err) {
                console.log(err);
            });

        }



        return false;
    }

    $scope.delGender = function (id) {
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $http({
                        url: 'http://softendev.lpru.ac.th/~prostd57115/data.php?page=del&id=' + id,
                        method: "GET",
                    }).then(function (res) {
                        if (res.data.success) {
                            swal("Success", {
                                icon: "success",
                            });
                        }
                        $scope.getGender();
                    }, function errorCallback(err) {
                        console.log(err);
                    });
                } else {
                    swal("Your imaginary file is safe!");
                }
            });
    }

    $scope.getEditData = function (id, index) {
        $scope.genderId = id;
        $scope.gender.name = $scope.dataGender[index].name;
    }


    $scope.renderChart = function () {
        $http({
            url: 'http://softendev.lpru.ac.th/~prostd57115/data.php?page=report',
            method: "GET",
        }).then(function (res) {
            var ctx = document.getElementById("gender").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: res.data.labels,
                    datasets: [{
                        label: '# of Votes',
                        data: res.data.data,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        }, function errorCallback(err) {

        });
    }

    $scope.renderChart();
    $scope.getGender();
}]);