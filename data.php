<?php  
include('func.php');
$func = new func();
$pdo = $func->connect();




if(isset($_REQUEST['page'])){
    if($_REQUEST['page'] == 'getData'){
        $data = array();
        $query = $pdo->prepare("SELECT * FROM gender ORDER BY id DESC");
        if($query->execute()){
            $data = $query->fetchAll(PDO::FETCH_ASSOC);
        }
        echo json_encode($data);
    }else if($_REQUEST['page'] == 'add'){
        $name = $_REQUEST['gender']['name'];
        $date = date("Y-m-d G:i:s");

        $query = $pdo->prepare("INSERT INTO gender SET name = '$name', create_at='$date', update_at='$date' ");
        if($query->execute()){
            echo json_encode(array(
                "success" => true,
                "message" => 'Success'
            ));
        }else{
            echo json_encode(array(
                "success" => false,
                "message" => 'Error'
            ));
        }
        
    }else if($_REQUEST['page'] == 'del'){
        $id = $_REQUEST['id'];

        $query = $pdo->prepare("DELETE FROM gender WHERE id = '$id'");
        if($query->execute()){
            echo json_encode(array(
                "success" => true,
                "message" => 'Success'
            ));
        }else{
            echo json_encode(array(
                "success" => false,
                "message" => 'Error'
            ));
        }
    }else if($_REQUEST['page'] == 'edit'){
        $id = $_REQUEST['id'];
        $name = $_REQUEST['gender']['name'];
        $date = date("Y-m-d G:i:s");

        $query = $pdo->prepare("UPDATE gender SET name = '$name', update_at='$date' WHERE id = '$id' ");
        if($query->execute()){
            echo json_encode(array(
                "success" => true,
                "message" => 'Success'
            ));
        }else{
            echo json_encode(array(
                "success" => false,
                "message" => 'Error'
            ));
        }
    }else if($_REQUEST['page'] == 'report'){

        $query = $pdo->prepare("SELECT * FROM gender ORDER BY id DESC");
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);

        $label = array();
        $genderData = array();

        foreach($data as $dt){
            array_push($label,$dt['name']);
            
        }
        $label = array_unique($label);
        sort($label);

        for($i=0;$i<count($label);$i++){
            $genderData[$i] = 0;
            foreach($data as $dt){
                if($dt['name'] == $label[$i]){
                    $genderData[$i]++;
                }
            }
        }     

        echo json_encode(array(
            "labels" => $label,
            "data" => $genderData
        ));

    }
}




?>